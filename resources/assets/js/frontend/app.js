import '../bootstrap.js';

import dashboard from './Dashboard';
import NotFoundComponent from './notFoundComponent';
import love from './Love';
import store from './store/store.js';
import Papp from './Papp';

import VueRouter from  'vue-router';

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '*', component: NotFoundComponent
    },
    {
      path: '/vue/dashboard',
      component: dashboard,
      name: 'dashboard',
    },
    {
      path: '/vue/dashboard/love',
      component: love,
      name: 'love',
    }
  ]
})

const vm = new Vue({
  el:'#vue',
  router,
  store,
  render: h =>h(Papp),
  // template: `
  //   <div>
  //   <ul>
  //     <li>
  //       <router-link to ="/vue/dashboard">Vue</router-link>
  //     </li>
  //     <li>
  //       <router-link to ="/vue/dashboard/love">Love</router-link>
  //     </li>
  //   </ul>
  //   <transition name="fade" mode="out-in">
  //     <router-view></router-view>
  //   </transition>
  //   </div>
  // `
});
console.log(vm, 'vm');
console.log(store, 'store');
