const mix = require('laravel-mix');

mix.js('resources/assets/js/backend/app.js', 'public/assets/js/backend')
	.js('resources/assets/js/frontend/app.js', 'public/assets/js/frontend')
	.sass('resources/assets/sass/frontend/app.scss', 'public/assets/css/frontend')
	.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/**','public/assets/css/fonts/bootstrap')
	.setPublicPath('public')
	// .version()
    .options({
        processCssUrls: false,
    });
